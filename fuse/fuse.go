package fuse

import (
	"bazil.org/fuse"
	"bazil.org/fuse/fs"
	"bitbucket.org/daphee/tutoco/server"
	"bytes"
	"log"
	"os"
	"strings"
)

func Start() {
	c, err := fuse.Mount("/var/tutoco")
	if err != nil {
		log.Fatal(err)
	}
	defer c.Close()

	err = fs.Serve(c, FS{})
	if err != nil {
		log.Fatal(err)
	}

	// check if the mount process has an error to report
	<-c.Ready
	if err := c.MountError; err != nil {
		log.Fatal(err)
	}

	log.Println("Fuse exiting")
}

type FS struct{}

func (FS) Root() (fs.Node, fuse.Error) {
	return Root{}, nil
}

type Root struct{}

func (Root) Attr() fuse.Attr {
	return fuse.Attr{Mode: os.ModeDir | 0777}
}

func (Root) Lookup(name string, intr fs.Intr) (fs.Node, fuse.Error) {
	log.Println("Lookup Root")
	if exists, err := server.DomainExists(name); err != nil {
		log.Println("Error: Root-Lookup: DomainExists:", err)
		return nil, fuse.EIO
	} else if exists {
		return Domain{Name: name}, nil
	}

	return nil, fuse.ENOENT
}

func (Root) ReadDir(intr fs.Intr) ([]fuse.Dirent, fuse.Error) {
	domains, err := server.GetDomains()
	if err != nil {
		log.Println("Error: ReadDir-Root: GetDomains:", err)
		return []fuse.Dirent{}, fuse.EIO
	}

	dirs := []fuse.Dirent{}

	for _, domain := range domains {
		dirs = append(dirs, fuse.Dirent{Type: fuse.DT_Dir, Name: domain})
	}

	return dirs, nil
}

type Domain struct {
	Name string
}

func (d Domain) Attr() fuse.Attr {
	return fuse.Attr{Mode: os.ModeDir | 0777}
}

func (d Domain) ReadDir(intr fs.Intr) ([]fuse.Dirent, fuse.Error) {
	sources, err := server.GetSources(d.Name)
	if err != nil {
		log.Println("Error: ReadDir-Domain: GetSources:", err)
		return []fuse.Dirent{}, fuse.EIO
	}

	entrys := []fuse.Dirent{}

	for _, source := range sources {
		entrys = append(entrys, fuse.Dirent{Type: fuse.DT_File, Name: source})
	}

	return entrys, nil
}

func (d Domain) Lookup(name string, intr fs.Intr) (fs.Node, fuse.Error) {
	if exists, err := server.SourceExists(d.Name, name); err != nil {
		log.Println("Error: Domain-Lookup: SOurceExists:", err)
		return nil, fuse.EIO
	} else if exists {
		return Source{Domain: d.Name, Name: name}, nil
	} else if !exists && strings.LastIndex(name, ":") > 0 {
		newName := name[0:strings.LastIndex(name, ":")]
		attr := name[strings.LastIndex(name, ":")+1:]
		if exists, err = server.SourceExists(d.Name, newName); err != nil {
			log.Println("Error: Domain-Lookup: SourceExists2:", err)
			return nil, fuse.EIO
		} else if exists {
			return Source{Domain: d.Name, Name: newName, Attribute: attr}, nil
		}
	}

	return nil, fuse.ENOENT
}

func (d Domain) Remove(req *fuse.RemoveRequest, intr fs.Intr) fuse.Error {
	if exists, err := server.SourceExists(d.Name, req.Name); err != nil {
		log.Println("Error: Remove: SourceExists:", err)
		return fuse.EIO
	} else if !exists {
		return fuse.ENOENT
	}

	if err := server.Remove(d.Name, req.Name); err != nil {
		log.Println("Error: Remove: Remove:", err)
		return fuse.EIO
	}
	return nil
}

type Source struct {
	Domain, Name, Attribute string
}

func (s Source) Attr() fuse.Attr {
	return fuse.Attr{Mode: 0777}
}

func (s Source) ReadAll(intr fs.Intr) ([]byte, fuse.Error) {
	val, err := server.ReadSource(s.Domain, s.Name, s.Attribute)
	if err != nil {
		log.Println("Error: ReadAll: ReadSource:", err)
		return []byte{}, fuse.EIO
	}
	return val, nil
}

//maybe just ignore thos req.Flags?...
func (s Source) Open(req *fuse.OpenRequest, resp *fuse.OpenResponse, intr fs.Intr) (fs.Handle, fuse.Error) {
	return SourceHandle{Source: s, Data: &bytes.Buffer{}}, nil
}

type SourceHandle struct {
	Source
	Data *bytes.Buffer
}

func (s SourceHandle) Write(req *fuse.WriteRequest, resp *fuse.WriteResponse, intr fs.Intr) fuse.Error {
	n, err := s.Data.Write(req.Data)
	if err != nil {
		return fuse.EIO
	}
	resp.Size = n
	return nil
}

//only write when flushed
func (s SourceHandle) Flush(req *fuse.FlushRequest, intr fs.Intr) fuse.Error {
	if s.Data.String() != "" {
		server.WriteSource(s.Domain, s.Name, s.Attribute, s.Data.String())
	}
	return nil
}
