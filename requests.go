package server

import (
	"encoding/json"
	"errors"
)

func GetDomains() ([]string, error) {
	resp, err := Send(&Message{Action: "get_domains"})
	if err != nil {
		return []string{}, err
	} else {
		domains := []string{}
		if err := json.Unmarshal(*resp.Content.(*json.RawMessage), &domains); err != nil {
			return []string{}, err
		}
		return domains, nil
	}
}

func DomainExists(searchDomain string) (bool, error) {
	domains, err := GetDomains()
	if err != nil {
		return false, err
	}

	for _, domain := range domains {
		if domain == searchDomain {
			return true, nil
		}
	}

	return false, nil
}

type GetSourcesRequest struct {
	Domain string `json:"domain"`
}

func GetSources(domain string) ([]string, error) {
	resp, err := Send(&Message{Action: "get_sources", Content: GetSourcesRequest{Domain: domain}})
	if err != nil {
		return []string{}, err
	} else {
		sources := []string{}
		if err := json.Unmarshal(*resp.Content.(*json.RawMessage), &sources); err != nil {
			return []string{}, err
		}
		return sources, nil
	}
}

func SourceExists(domain, searchSource string) (bool, error) {
	sources, err := GetSources(domain)
	if err != nil {
		return false, err
	}

	for _, source := range sources {
		if source == searchSource {
			return true, nil
		}
	}

	return false, nil
}

type ReadRequest struct {
	Domain    string `json:"domain"`
	Source    string `json:"source"`
	Attribute string `json:"attribute,omitempty"`
}

type WriteRequest struct {
	ReadRequest
	Data string `json:"data"`
}

func ReadSource(domain, source, attribute string) ([]byte, error) {
	resp, err := Send(&Message{Action: "read", Content: ReadRequest{Domain: domain, Source: source, Attribute: attribute}})
	if err != nil {
		return []byte{}, err
	} else {
		val := ""
		if err := json.Unmarshal(*resp.Content.(*json.RawMessage), &val); err != nil {
			return []byte{}, err
		}
		return []byte(val), nil
	}
}

func WriteSource(domain, source, attribute, data string) {
	SendWithoutResponse(&Message{
		Action:  "write",
		Content: WriteRequest{ReadRequest: ReadRequest{Domain: domain, Source: source, Attribute: attribute}, Data: data},
	})
}

type SuccessResponse struct {
	Success bool   `json:"success"`
	Message string `json:"msg"`
}

func Remove(domain, source string) error {
	resp, err := Send(&Message{Action: "remove", Content: ReadRequest{Domain: domain, Source: source}})
	if err != nil {
		return err
	} else {
		success := &SuccessResponse{}
		if err := json.Unmarshal(*resp.Content.(*json.RawMessage), success); err != nil {
			return err
		}

		if !success.Success {
			return errors.New(success.Message)
		}
		return nil
	}
}
