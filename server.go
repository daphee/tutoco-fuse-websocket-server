package server

import (
	"code.google.com/p/go.net/websocket"
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"io"
	"log"
	"time"
)

var requests = make(map[int]chan *Message)

var Router = mux.NewRouter()

type Message struct {
	Action  string      `json:"action"`
	Id      int         `json:"id"`
	Content interface{} `json:"data"`
}

var connection *websocket.Conn
var isBusy = false

var netmanSend chan *SendMessage

type SendMessage struct {
	Response chan *Message
	Message  *Message
}

var netmanReceive chan *Message
var idCounter = 0

func Netman() {
	netmanSend = make(chan *SendMessage)
	netmanReceive = make(chan *Message)

	log.Println("Netmanager starting")

	go func() {
		for {
			select {
			case tosend := <-netmanSend:
				if !isBusy {
					continue
				}
				idCounter++
				tosend.Message.Id = idCounter
				requests[tosend.Message.Id] = tosend.Response
				if err := websocket.JSON.Send(connection, tosend.Message); err != nil {
					log.Println("Error: Netman: Send:", err)
					delete(requests, tosend.Message.Id)
				}

			case msg := <-netmanReceive:
				if c, ok := requests[msg.Id]; ok {
					c <- msg
					close(c)
					delete(requests, msg.Id)
				}
			}
		}
	}()

	return
}

func WebSocketHandler(ws *websocket.Conn) {
	if isBusy {
		ws.Close()
	}

	connection = ws
	isBusy = true
	log.Println("Client connected")
	for {
		msg := &Message{Content: new(json.RawMessage)}
		if err := websocket.JSON.Receive(ws, msg); err != nil {
			if err != io.EOF {
				log.Println("Error: WebSocketHandler: Receive:", err)
			}
			break
		}

		log.Println("Got Message:", msg)

		netmanReceive <- msg
	}
	log.Println("Client disconnected")
	ws.Close()
	isBusy = false
	connection = nil
}

func Send(msg *Message) (*Message, error) {
	if !isBusy {
		return &Message{}, errors.New("Send: No connection")
	}

	c := make(chan *Message)

	netmanSend <- &SendMessage{Response: c, Message: msg}

	select {
	case resp := <-c:
		return resp, nil
	case <-time.After(10 * time.Second):
		return &Message{}, errors.New("Send: Receive: Timeout(10s)")
	}
}

func SendWithoutResponse(msg *Message) {
	if !isBusy {
		return
	}

	if err := websocket.JSON.Send(connection, msg); err != nil {
		log.Println("Error: SendWithoutResponse: Send:", err)
	}
}
